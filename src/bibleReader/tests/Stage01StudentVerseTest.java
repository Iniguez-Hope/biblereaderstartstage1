package bibleReader.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import bibleReader.model.BookOfBible;
import bibleReader.model.Reference;
import bibleReader.model.Verse;

/*
 * Tests for the Verse class.
 * @author Alexander Buaer and Angel Iniguez Jr
 * @verson 2.4.2019
 */

public class Stage01StudentVerseTest {

	// Setup the Reference Fields.
	private Reference ruth2_4;
	private Reference gen1_3;
	private Reference rev5_12;
	private Reference mark6_5;
	private Reference psalms130_1;
	private Reference exodus1_3;
	// Setup Verse with the second constructor.
	private Verse ruth2_4Verse;
	private Verse gen1_3Verse;
	private Verse rev5_12Verse;
	private Verse mark6_5Verse;
	private Verse psalms130_1Verse;
	private Verse exodus1_3Verse;
	// Used for testing sameReference Method
	private Verse ruth2_4Dummy;
	private Verse gen1_3Dummy;
	// used to test the equals method
	private Verse ruth2_4_2;
	private Verse gen1_3_2;

	/*
	 * Anything that should be done before each test. For instance, you might create
	 * some objects here that are used by several of the tests.
	 */
	@Before
	public void setUp() throws Exception {
		// Initialize the Reference Fields.
		ruth2_4 = new Reference(BookOfBible.Ruth, 2, 4);
		gen1_3 = new Reference(BookOfBible.Genesis, 1, 3);
		rev5_12 = new Reference(BookOfBible.Revelation, 5, 12);
		mark6_5 = new Reference(BookOfBible.Mark, 6, 5);
		psalms130_1 = new Reference(BookOfBible.Psalms, 130, 1);
		exodus1_3 = new Reference(BookOfBible.Exodus, 1, 3);
		// Initialize the Verses.
		ruth2_4Verse = new Verse(ruth2_4, "Just then Boaz arrived from Bethlehem and greeted the harvesters, "
				+ "'The Lord be with you!' 'The Lord bless you!' they answered.");
		gen1_3Verse = new Verse(gen1_3, "And God said, 'Let there be light,' and there was light.");
		rev5_12Verse = new Verse(rev5_12, "In a loud voice they were saying: 'Worthy is the Lamb, who was slain,"
				+ "to receive power and wealth and wisdom and strength and honor and glory and praise!'");
		mark6_5Verse = new Verse(mark6_5,
				"He could not do any miracles there, except lay his hands on a few sick people " + "and heal them.");
		psalms130_1Verse = new Verse(psalms130_1, "Out of the depths I cry to you, Lord;");
		exodus1_3Verse = new Verse(exodus1_3, "Issachar, Zebulun and Benjamin;");
		// Initialize the extra verses.
		ruth2_4Dummy = new Verse(ruth2_4, "Hey this is wrong");
		gen1_3Dummy = new Verse(gen1_3, "This one is also wrong");
		//These are testing the second Constructor.
		ruth2_4_2 = new Verse(BookOfBible.Ruth, 2, 4,
				"Just then Boaz arrived from Bethlehem and greeted the harvesters, "
						+ "'The Lord be with you!' 'The Lord bless you!' they answered.");
		gen1_3_2 = new Verse(BookOfBible.Genesis, 1, 3, "And God said, 'Let there be light,' and there was light.");
	}

	/*
	 * Anything that should be done at the end of each test. Most likely you won't
	 * need to do anything here.
	 */
	@After
	public void tearDown() throws Exception {
		// You probably won't need anything here.
	}

	/*
	 * Add as many test methods as you think are necessary. Two suggestions (without
	 * implementation) are given below.
	 */

	/**
	 * Testing the getters in the Verse Class
	 */
	@Test
	public void testGetters() {
		// Testing the getReference Method
		assertEquals(ruth2_4Verse.getReference(), ruth2_4);
		assertEquals(gen1_3Verse.getReference(), gen1_3);

		// Testing get text method
		assertEquals(gen1_3Verse.getText(), "And God said, 'Let there be light,' and there was light.");
		assertEquals(psalms130_1Verse.getText(), "Out of the depths I cry to you, Lord;");
	}

	/**
	 * Testing the ToString method in Verse
	 */
	@Test
	public void testToString() {
		assertEquals(rev5_12Verse.toString(), rev5_12.toString() + " " + rev5_12Verse.getText());
		assertEquals(mark6_5Verse.toString(), mark6_5.toString() + " " + mark6_5Verse.getText());
	}

	/**
	 * Testing the Equals method in Verse and the 2 constructors
	 */
	@Test
	public void testEquals() {
		// Testing for equality
		assertEquals(ruth2_4Verse.getReference(), ruth2_4_2.getReference());
		assertEquals(ruth2_4Verse.getText(), ruth2_4_2.getText());
		assertEquals(gen1_3Verse.getReference(), gen1_3_2.getReference());
		assertEquals(gen1_3Verse.getText(), gen1_3_2.getText());

		// Testing to make sure that they fail.
		assertEquals(ruth2_4Verse.getReference(), ruth2_4Dummy.getReference());
		assertNotEquals(ruth2_4Verse.getText(), ruth2_4Dummy.getText());
	}

	/**
	 * Testing the HashCode method in Verse
	 */
	@Test
	public void testHashCode() {
		assertEquals(mark6_5Verse.hashCode(), mark6_5Verse.toString().hashCode());
		assertEquals(psalms130_1Verse.hashCode(), psalms130_1Verse.toString().hashCode());
	}

	/**
	 * Testing the compareTo Method in Verse
	 */
	@Test
	public void testCompareTo() {
		// make sure that they return negative number
		assertEquals(gen1_3Verse.compareTo(psalms130_1Verse), -18);
		assertEquals(ruth2_4.compareTo(mark6_5), -33);

		// Make sure that they return 0
		assertEquals(ruth2_4Verse.compareTo(ruth2_4_2), 0);
		assertEquals(gen1_3Verse.compareTo(gen1_3_2), 0);

		// Make sure that they return a positive number
		assertEquals(rev5_12Verse.compareTo(exodus1_3Verse), 64);
		assertEquals(psalms130_1Verse.compareTo(ruth2_4Verse), 11);
	}

	/**
	 * Testing the sameReference method in Verse
	 */
	@Test
	public void testSameReference() {
		// Test that they have the same Reference
		assertTrue(ruth2_4Verse.sameReference(ruth2_4Dummy));
		assertTrue(gen1_3Verse.sameReference(gen1_3Dummy));
		// Test that they have different References.
		assertFalse(rev5_12Verse.sameReference(mark6_5Verse));
		assertFalse(psalms130_1Verse.sameReference(mark6_5Verse));
	}
}
